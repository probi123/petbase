import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:pawfinder/settings.dart';
import 'package:pawfinder/src/model/announcement/comment.dart';
import 'package:pawfinder/src/model/announcement/pet_announcement.dart';
import 'package:pawfinder/src/model/user/user.dart';
import 'package:test/test.dart';

void main() {
  test("test for parsing user", () async {
    final response = await http.get(
      'https://find-pet-app.herokuapp.com/rest/user/3',
      headers: {
        HttpHeaders.authorizationHeader:
            "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJrcnp5c3p0b2YyIiwic2NvcGVzIjpbIkFETUlOIiwiVVNFUiJdLCJleHAiOjE1NjAyNTE3NDR9.Sb4x2UgOW-uvl0vV-QV3PUmsfOaxhAr4eS_9Vhi6LpNlUYXg4u1YWU1FtTFdKCWGos3t48EVpq7BCjCVTPtg_Q"
      },
    );
    final responseJson = json.decode(response.body);

    print(responseJson);

    User user = User.fromJson(responseJson);

    print(user);
  });

  test("test for parsing comment", () async {
    final response = await http.get(
      'https://find-pet-app.herokuapp.com/rest/comment/59',
      headers: {
        HttpHeaders.authorizationHeader:
            "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJrcnp5c3p0b2YyIiwic2NvcGVzIjpbIkFETUlOIiwiVVNFUiJdLCJleHAiOjE1NjAyNTE3NDR9.Sb4x2UgOW-uvl0vV-QV3PUmsfOaxhAr4eS_9Vhi6LpNlUYXg4u1YWU1FtTFdKCWGos3t48EVpq7BCjCVTPtg_Q"
      },
    );
    final responseJson = json.decode(response.body);

    print(responseJson);

    Comment comment = Comment.fromJsonWithDefaultDate(responseJson);

    print(comment);
  });

  test("test for parsing announcement", () async {
    final response = await http.get(
      'https://find-pet-app.herokuapp.com/rest/announcement/2',
      headers: {
        HttpHeaders.authorizationHeader:
            "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJrcnp5c3p0b2YyIiwic2NvcGVzIjpbIkFETUlOIiwiVVNFUiJdLCJleHAiOjE1NjAyNTE3NDR9.Sb4x2UgOW-uvl0vV-QV3PUmsfOaxhAr4eS_9Vhi6LpNlUYXg4u1YWU1FtTFdKCWGos3t48EVpq7BCjCVTPtg_Q"
      },
    );
    final responseJson = json.decode(response.body);

    print(responseJson);

    PetAnnouncement announcement = PetAnnouncement.fromJson(responseJson);

    print(announcement);

  });

  List<PetAnnouncement> _getListOfPetAnnouncements(responseBody) {
    List<PetAnnouncement> list = List<PetAnnouncement>();

    for (int i = 0; i < responseBody.length; i++) {
      list.add(PetAnnouncement.fromJson(responseBody[i]));
    }

    return list;
  }

  test("another test ", () async {
    final response = await http.get(
      Settings.ANNOUNCMENT_REST_URL + 'all',
      headers: {HttpHeaders.authorizationHeader: Settings.JWT_TOKEN},
    );

    final responseBody = json.decode(response.body);
    print(responseBody);

    List<PetAnnouncement> announcements =
        _getListOfPetAnnouncements(responseBody);
    print(announcements);

    return announcements;
  });
}
