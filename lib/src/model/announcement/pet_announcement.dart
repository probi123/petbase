import 'package:pawfinder/src/model/announcement/announcement_time.dart';
import 'package:pawfinder/src/model/announcement/comment.dart';
import 'package:pawfinder/src/model/announcement/location.dart';

const String DEFAULT_URL =
    "https://upload.wikimedia.org/wikipedia/commons/b/b6/Felis_catus-cat_on_snow.jpg";
const String MOCK_DESC =
    "Znaleziono kotka, na ulicy kociej 23, ma obrożę, dzwoneczek i trąbke w dupie. Prosze nie grać, kotek tego nie lubi.";

class PetAnnouncement {
  final int id;
  final int userId;

  final String title;
  String description;
  bool isActive;

  final AnnouncementTime date;
  Location location;

  String status;
  String type;

  String photoUrl; // should be List<String>
  List<Comment> comments;
  List<String> colors;

  // TODO: check parameters number
  PetAnnouncement(
      {this.location,
      this.id,
      this.userId,
      this.title,
      this.date,
      this.description,
      petPhotoUrl}) {
    photoUrl = petPhotoUrl ?? DEFAULT_URL;
  }

  PetAnnouncement.fromJson(Map<String, dynamic> json)
      : location = Location(json['latitude'], json['longitude']),
        comments = _parseComments(json['comments']),
        date = _parseDate(json['localDateTime']),
        title = json['title'],
        userId = json['userId'],
        id = json['id'],
        photoUrl = json['photoURL'][0],
        isActive = json['active'],
        type = json['animalType'],
        status = json['status'],
        description = json['description'];

  static AnnouncementTime _parseDate(String stringDate) =>
      AnnouncementTime(DateTime.parse(stringDate));

  static List<Comment> _parseComments(List<dynamic> commentsJson) {
    List<Comment> list = List<Comment>();
    for (int i = 0; i < commentsJson.length; i++) {
      list.add(Comment.fromJsonWithDefaultDate(commentsJson[i]));
    }
    return list;
  }

  PetAnnouncement.withDefaultText(this.id, this.userId, this.title, this.date,
      {petPhotoUrl}) {
    this.description = MOCK_DESC;
    this.location = Location(19.2, -47.2);
    photoUrl = petPhotoUrl ?? DEFAULT_URL;
  }

  String get userLabel => userId.toString();

  String get locationLabel => location.toString();

  String get timeLabel => date.toString();

  String get textLabel => '${description.substring(0, 15)}...';

  String get titleLabel => title.toUpperCase();

  @override
  String toString() => '''PetAnnouncement{   id: $id, 
                                             userId: $userId,
                                             title: $title, 
                                             desc: $description, 
                                             location: $location, 
                                             isActive: $isActive, 
                                             date: $date, 
                                             photoUrl: $photoUrl, 
                                             status: $status, 
                                             type: $type, 
                                             comments: $comments}''';
}

//class Status {
//  static const int LOST = 1;
//  static const int FOUND = 2;
//}
//
//class Type {
//  static const String DOG = "pies";
//  static const String CAT = "kot";
//  static const String SNAKE = "";
//}
