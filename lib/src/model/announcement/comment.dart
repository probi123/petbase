import 'package:pawfinder/src/model/announcement/announcement_time.dart';

class Comment {
  final int _commentId;
  final int _announcementId;
  final int _userId;

  final AnnouncementTime _dateTime;
  String _description;
  String _userName;

  Comment(this._commentId, this._announcementId, this._userId, this._dateTime,
      this._description, this._userName);

  Comment.fromJson(Map<String, dynamic> json)
      : _commentId = json['id'],
        _announcementId = json['announcementId'],
        _userId = json['userId'],
        _description = json['description'],
        _dateTime = _parseDate(json['date']),
        _userName = json['userName'];

  Comment.fromJsonWithDefaultDate(Map<String, dynamic> json)
      : _commentId = json['id'],
        _announcementId = json['announcementId'],
        _userId = json['userId'],
        _description = json['description'],
        _dateTime = AnnouncementTime(DateTime.now()),
        _userName = json['userName'];

  String get description => _description;

  AnnouncementTime get dateTime => _dateTime;

  static AnnouncementTime _parseDate(String stringDate) =>
      AnnouncementTime(DateTime.parse(stringDate));

  @override
  String toString() {
    return 'Comment{_commentId: $_commentId, _announcementId: $_announcementId, _userId: $_userId, _dateTime: $_dateTime, _description: $_description, _userName: $_userName}';
  }
}
