class AnnouncementTime {
  final int _dayVal;
  final int _monthVal;
  final int _yearVal;
  final int _hourVal;
  final int _minuteVal;

  AnnouncementTime(DateTime veryMoment)
      : _dayVal = veryMoment.day,
        _monthVal = veryMoment.month,
        _yearVal = veryMoment.year,
        _hourVal = veryMoment.hour,
        _minuteVal = veryMoment.minute;

  String toString() => '$_dayVal.$_monthVal.$_yearVal $_hourVal:$_minuteVal';
}
