class Location {
  //  final String _locationName;
  final double _latitude;
  final double _longitude;

  Location(this._latitude, this._longitude);
  //  Location(this._locationName, this._latitude, this._longitude);

  //  String get location => _locationName;
  double get lat => _latitude;
  double get long => _longitude;

  String toString() => '$_latitude,$_longitude';
}
