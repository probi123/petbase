class User {
  final int id;
  final String userName;
  final String email;
  final String password;

  User({this.id, this.userName, this.email, this.password});

  User.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        userName = json['nick'],
        email = json['email'],
        password = json['password'];

  String get name => userName;

  Map<String, dynamic> toJson() {
    return {
      'name': userName,
      'email': email,
      'password': password,
    };
  }

  String toString() => '$userName $email';
}
