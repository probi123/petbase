import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:pawfinder/src/widgets/bottom_navigation_bar.dart';

class Bar extends StatefulWidget {
  @override
  _BarState createState() => _BarState();
}

class _BarState extends State<Bar> {
  File image;

  Future<void> picker() async {
    File img = await ImagePicker.pickImage(source: ImageSource.camera);

    if (img != null) {
      image = img;
      setState(() {
        Navigator.pop(context, img);
      });
    }
  }
  
  @override
  Widget build(BuildContext context) {
    const APP_NAME = 'PawFinder';

    return Container(
      decoration: BoxDecoration(color: Theme.of(context).primaryColor),
      height: MediaQuery.of(context).size.height / 1.5,
      width: MediaQuery.of(context).size.width / 1.5,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          IconButton(
            padding: EdgeInsets.only(top: 8),
            icon: Icon(
              Icons.camera_alt,
              size: 30,
            ),
            iconSize: 30,
            onPressed: picker,
          ),
          FlatButton(
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            child: Text(
              APP_NAME,
              style: TextStyle(
                  fontFamily: 'Lobster', color: const Color(0xFF89CDC4)),
              textScaleFactor: 2.0,
            ),
            onPressed: () {Navigator.pushNamedAndRemoveUntil(
                context, '/home', (Route<dynamic> route) => false);
                // BottomBarState.currentIndex = 0;
                },
          )
        ],
      ),
    );
  }
}
