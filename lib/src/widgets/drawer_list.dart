import 'package:flutter/material.dart';
import 'package:pawfinder/src/util/lang/pawfinder_localizations.dart';

class DrawerList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 5.0),
        children: <Widget>[
          ListTile(
            title: Text(PawfinderLocalizations.of(context)
                .getTranslation('my_account')),
            trailing: Icon(Icons.account_circle),
            onTap: () {
              Navigator.of(context).pop();
              Navigator.pushReplacementNamed(context, '/account');
            },
          ),
          ListTile(
            title: Text(
                PawfinderLocalizations.of(context).getTranslation('main_page')),
            trailing: Icon(Icons.home),
            onTap: () => Navigator.pushNamedAndRemoveUntil(
                context, '/home', (Route<dynamic> route) => false),
          ),
          ListTile(
            title: Text(
                PawfinderLocalizations.of(context).getTranslation('lost_pets')),
            trailing: Icon(Icons.arrow_forward),
            onTap: () =>
                Navigator.pushReplacementNamed(context, '/filtered/lost'),
          ),
          ListTile(
            title: Text(PawfinderLocalizations.of(context)
                .getTranslation('found_pets')),
            trailing: Icon(Icons.arrow_forward),
            onTap: () =>
                Navigator.pushReplacementNamed(context, '/filtered/found'),
          ),
          ListTile(
            title: Text(
                PawfinderLocalizations.of(context).getTranslation('seen_pets')),
            trailing: Icon(Icons.arrow_forward),
            onTap: () =>
                Navigator.pushReplacementNamed(context, '/filtered/seen'),
          ),
          ListTile(
            title: Text(
                PawfinderLocalizations.of(context).getTranslation('logout')),
            trailing: Icon(Icons.exit_to_app),
            onTap: () => Navigator.pushReplacementNamed(context, '/logout'),
          ),
          ListTile(
            title:
                Text(PawfinderLocalizations.of(context).getTranslation('help')),
            trailing: Icon(Icons.help),
            onTap: () => Navigator.pushReplacementNamed(context, '/help'),
          ),
        ],
      ),
    );
  }
}
