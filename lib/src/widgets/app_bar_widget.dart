import 'package:flutter/material.dart';

import 'package:pawfinder/src/widgets/bar.dart';

class AppBarWidget extends AppBar {
  AppBarWidget({Key key, Widget title, List<String> tabs})
      : super(
          title: Bar(),
          bottom: _initBar(tabs),
          elevation: 0.0,
          actions: [
            Builder(
              builder: (context) => IconButton(
                  icon: Icon(
                    // Icons.pets,
                    Icons.account_circle,
                    size: 30,
                  ),
                  onPressed: () => Navigator.of(context)
                      .pushNamedAndRemoveUntil(
                          "/account",
                          (route) => route.isCurrent
                              ? route.settings.name == "/account" ? false : true
                              : true)
                  // onPressed: () => Scaffold.of(context).openEndDrawer(),
                  // tooltip:
                  //     MaterialLocalizations.of(context).openAppDrawerTooltip,
                  ),
            ),
          ],
        );

  static TabBar _initBar(List<String> tabs) {
    //   if (tabs == null) {
    //     return null;
    //   } else {
    //     List<Widget> tabList = [];
    //     for (var i in tabs) {
    //       tabList.add(Tab(
    //         text: i,
    //       ));
    //     }
    //     return TabBar(
    //       tabs: tabList,
    //       indicatorColor: const Color(0xFF89CDC4),
    //       labelColor: const Color(0xFF89CDC4),
    //       unselectedLabelColor: Colors.black38,
    //     );
    //   }
    // }
  }
}
