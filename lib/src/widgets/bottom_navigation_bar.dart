import 'package:flutter/material.dart';

class BottomBar extends StatefulWidget {
  Function onTap;
  int currentIndex = 0;
  List views;

  BottomBar({@required this.currentIndex, this.onTap, @required this.views});

  @override
  State<StatefulWidget> createState() {
    return BottomBarState();
  }
}

class BottomBarState extends State<BottomBar> {
  @override
  Widget build(BuildContext context) {
    var bottomNavigationBar = BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: widget.currentIndex,
        onTap: widget.onTap,
        items: widget.views
            .map((element) => BottomNavigationBarItem(
                title: Text(element[0]), icon: element[2]))
            .toList());
    return new Theme(
        data: Theme.of(context).copyWith(
          canvasColor: Theme.of(context).backgroundColor,
        ),
        child: bottomNavigationBar);
  }


}
