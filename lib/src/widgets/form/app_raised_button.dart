import 'package:flutter/material.dart';
import 'package:pawfinder/src/util/lang/pawfinder_localizations.dart';

class AppRaisedButton extends StatefulWidget {
  // Members:
  String label;
  Function() onPressed;

  // Methods:
  AppRaisedButton({this.label, this.onPressed});

  @override
  _AppRaisedButtonState createState() => _AppRaisedButtonState();
}

class _AppRaisedButtonState extends State<AppRaisedButton> {
  @override
  Widget build(BuildContext context) {
    // AppRaisedButton display settings:
    final Color _borderColor = Theme.of(context).accentColor;

    return RaisedButton(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
      child:
          Text(PawfinderLocalizations.of(context).getTranslation(widget.label)),
      color: Theme.of(context).accentColor,
      splashColor: Colors.blueGrey,
      onPressed: widget.onPressed,
    );
  }
}
