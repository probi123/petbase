import 'package:flutter/material.dart';
import 'package:pawfinder/src/util/lang/pawfinder_localizations.dart';

class TextFormInputPrefixIconOutline extends StatefulWidget {
  // Members:
  int lines;
  String label;
  String Function(String value) valdiator;
  IconData icon;
  dynamic Function(String value) onSavedFuntion;

  // Methods:
  TextFormInputPrefixIconOutline(
      {this.label, this.icon, this.valdiator, this.onSavedFuntion, this.lines});

  @override
  State<StatefulWidget> createState() {
    return _TextFormInputPrefixIconOutlineState();
  }
}

class _TextFormInputPrefixIconOutlineState
    extends State<TextFormInputPrefixIconOutline> {
  @override
  Widget build(BuildContext context) {
    // TextInputOutline display settigs:
    final Color _borderColor = Theme.of(context).accentColor;
    final Color _textColor = Colors.black;

    return TextFormField(
      maxLines: widget.lines,
      validator: widget.valdiator,
      onSaved: widget.onSavedFuntion,
      decoration: InputDecoration(
          prefixIcon: Icon(widget.icon),
          labelText:
              PawfinderLocalizations.of(context).getTranslation(widget.label),
          labelStyle: TextStyle(color: _textColor),
          focusedBorder:
              OutlineInputBorder(borderSide: BorderSide(color: _borderColor)),
          enabledBorder:
              OutlineInputBorder(borderSide: BorderSide(color: _borderColor))),
    );
  }
}
