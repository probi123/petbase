import 'package:flutter/material.dart';
import 'package:pawfinder/src/util/lang/pawfinder_localizations.dart';

class AppDropDownButton extends StatefulWidget {
  // Members:
  List<String> optionsArray;
  Function(dynamic vaue) onChangedFuntion;

  // Methods:
  AppDropDownButton({this.optionsArray, this.onChangedFuntion});

  @override
  _AppDropDownButtonState createState() => _AppDropDownButtonState();
}

class _AppDropDownButtonState extends State<AppDropDownButton> {
  @override
  Widget build(BuildContext context) {
    // AppDropDownButton display settings:
    final Color _borderColor = Theme.of(context).accentColor;

    // Translating array:

    // Returning Widget:
    return Container(
        padding: EdgeInsets.all(5),
        decoration: BoxDecoration(
            border: Border.all(color: _borderColor),
            borderRadius: BorderRadius.circular(5)),
        child: DropdownButton(
          items: widget.optionsArray
              .map(
                (element) => DropdownMenuItem(
                    child: Text(PawfinderLocalizations.of(context)
                        .getTranslation(element))),
              )
              .toList(),
          onChanged: (dynamic value) {}, //widget.onChangedFuntion,
        ));
  }
}
