import 'package:flutter/material.dart';
import 'package:pawfinder/src/util/lang/pawfinder_localizations.dart';

class TextFormInputOutline extends StatefulWidget {
  // Members:
  int lines;
  String label;
  String Function(String value) validator;
  IconData icon;
  dynamic Function(String value) onSaved;
  var keyboardType;
  var hintText;

  var obscureText;

  // Methods:
  TextFormInputOutline(
      {this.label,
      this.validator,
      this.onSaved,
      this.keyboardType,
      this.hintText,
      this.obscureText = false}) {
    label = label;
  }

  @override
  State<StatefulWidget> createState() {
    return _TextFormInputOutlineState();
  }
}

class _TextFormInputOutlineState extends State<TextFormInputOutline> {
  @override
  Widget build(BuildContext context) {
    // TextInputOutline display settigs:
    final Color _borderColor = Theme.of(context).accentColor;
    final Color _textColor = Colors.black;

    return TextFormField(
      obscureText: widget.obscureText,
      keyboardType: widget.keyboardType,
      maxLines: widget.lines,
      validator: widget.validator,
      onSaved: widget.onSaved,
      decoration: InputDecoration(
          hintText: widget.hintText,
          labelText:
              PawfinderLocalizations.of(context).getTranslation(widget.label),
          labelStyle: TextStyle(color: _textColor),
          focusedBorder:
              OutlineInputBorder(borderSide: BorderSide(color: _borderColor)),
          errorBorder:
              OutlineInputBorder(borderSide: BorderSide(color: Colors.red)),
          focusedErrorBorder:
              OutlineInputBorder(borderSide: BorderSide(color: Colors.red)),
          enabledBorder:
              OutlineInputBorder(borderSide: BorderSide(color: _borderColor))),
    );
  }
}
