import 'package:scoped_model/scoped_model.dart';
import 'dart:async';
import 'package:pawfinder/pawfinder_exceptions.dart';
import 'package:pawfinder/src/model/user/user.dart';
import 'package:pawfinder/src/util/services/auth_service.dart';

mixin AuthUserModel on Model {
  String _token = null;
  User _authUser = User(id: null, userName: null, email: null, password: null);

  User get authUser => this._authUser;
  String get token => this._token;

  Future login(String login, String password) async {
    AuthService authService = AuthService();
      Map<String, dynamic> callback =
          await authService.authenticate(login, password);
      this._token = callback["token"];
      this._authUser = callback["user"];
  }
}
