import 'package:flutter/material.dart';
import 'package:pawfinder/src/model/announcement/announcement_time.dart';
import 'package:pawfinder/src/model/announcement/pet_announcement.dart';
// import 'package:pawfinder/src/screens/all/pet_list_view.dart';
// import 'package:pawfinder/src/screens/all/pet_widget.dart';

class AllView extends StatelessWidget {
  final PetAnnouncement announcement1 = PetAnnouncement.withDefaultText(
      0,
      1, // TODO: in mock data need to create User() with this id in list view
      "Kot znaleziony",
      AnnouncementTime(DateTime.parse("2019-05-11T15:00:00")));

  final PetAnnouncement announcement2 = PetAnnouncement.withDefaultText(
      1,
      2,
      "Pies znaleziony",
      AnnouncementTime(DateTime.parse("2019-05-11T15:00:00")));

  final PetAnnouncement announcement3 = PetAnnouncement.withDefaultText(
      2,
      3,
      "WUNSZ znaleziony",
      AnnouncementTime(DateTime.parse("2019-05-11T15:00:00")));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
          child: Text("data"),
      // children: <Widget>[
      //   PetsListView(
      //     children: <PetWidget>[
      //       PetWidget(announcement1),
      //       PetWidget(announcement2),
      //       PetWidget(announcement3),
      //       PetWidget(announcement2),
      //     ],
      //   )
      // ],
      // )
    ));
  }
}
