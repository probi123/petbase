import 'package:flutter/material.dart';
import 'package:pawfinder/src/util/services/announcement_service.dart';

const double _imageWidth = 150;
const double _imageHeight = 100;
const double _widgetPadding = 10;
const double _photoPadding = 20;

class LostView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LostViewState();
}

class _LostViewState extends State<LostView> {
  final AnnouncementService service = AnnouncementService();
  @override
  Widget build(BuildContext context) {
    return Container(
      child: FutureBuilder(
        future: service.findLost(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.data == null) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            return ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index) {
                return FlatButton(
                  padding: EdgeInsets.all(0),
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  onPressed: () => Navigator.pushNamed(context, '/announcement',
                      arguments: snapshot.data[index]),
                  child: _buildCard(snapshot, index),
                );
              },
            );
          }
        },
      ),
    );
  }

  Widget _buildCard(AsyncSnapshot snapshot, int index) {
    return Card(
      color: Colors.white,
      child: Padding(
        padding: EdgeInsets.all(_widgetPadding),
        child: Row(
          children: <Widget>[
            _buildImage(snapshot, index),
            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  _buildTitle(snapshot, index),
                  _buildUserLabel(snapshot, index),
                  _buildLocationLabel(snapshot, index),
                  _buildTimeLabel(snapshot, index),
                  // _buildDescriptionArea(snapshot, index) // FIXME: <description is going behind padding/border>
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildImage(AsyncSnapshot snapshot, int index) {
    return Padding(
        padding: EdgeInsets.only(right: _photoPadding),
        child: ClipRRect(
            borderRadius: BorderRadius.circular(8.0),
            child: Image.network(
              snapshot.data[index].photoUrl,
              height: _imageHeight,
              width: _imageWidth,
            )));
  }

  Widget _buildTitle(AsyncSnapshot snapshot, int index) {
    return Padding(
      padding: EdgeInsets.only(bottom: 10),
      child: Text(
        snapshot.data[index].titleLabel,
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
    );
  }

  Widget _buildUserLabel(AsyncSnapshot snapshot, int index) {
    return Row(children: <Widget>[
      Icon(Icons.account_circle),
      Text(snapshot.data[index].userLabel),
    ]);
  }

  Widget _buildLocationLabel(AsyncSnapshot snapshot, int index) {
    return Row(children: <Widget>[
      Icon(Icons.location_on),
      Row(
        children: <Widget>[
          Text(snapshot.data[index].location.lat.toStringAsFixed(2)),
          Text(","),
          Text(snapshot.data[index].location.long.toStringAsFixed(2)),
        ],
      )
    ]);
  }

  Widget _buildTimeLabel(AsyncSnapshot snapshot, int index) {
    return Row(children: <Widget>[
      Icon(Icons.schedule),
      Text(snapshot.data[index].timeLabel),
    ]);
  }

  Widget _buildDescriptionArea(AsyncSnapshot snapshot, int index) {
    return Row(children: <Widget>[
      Icon(Icons.info),
      Text(snapshot.data[index].textLabel),
    ]);
  }
}
