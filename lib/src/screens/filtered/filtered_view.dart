import 'package:flutter/material.dart';
import 'package:pawfinder/src/screens/filtered/conrete/found_view.dart';
import 'package:pawfinder/src/screens/filtered/conrete/lost_view.dart';
import 'package:pawfinder/src/screens/filtered/conrete/seen_view.dart';

class FilteredView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _FilteredViewState();
  }
}

class _FilteredViewState extends State<FilteredView>
    with SingleTickerProviderStateMixin {
  final int initialTabIndex = 0;
  TabController _tabController;

  final Map<String, Widget> _tabs = {
    'ZAGUBIONE': LostView(),
    'ZNALEZIONE': FoundView(),
    'WIDZIANE': SeenView()
  };

  @override
  void initState() {
    _tabController = new TabController(length: _tabs.length, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: TabBar(
          tabs: _tabs.keys.map((e) => Tab(child: Text(e))).toList(),
          controller: _tabController,
        ),
        body: Container(
          child: TabBarView(
            controller: _tabController,
            children: _tabs.values.toList(),
          ),
        ));
  }
}
