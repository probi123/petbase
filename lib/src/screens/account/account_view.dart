import 'package:flutter/material.dart';
import 'package:pawfinder/src/widgets/app_bar_widget.dart';
import 'package:pawfinder/src/widgets/drawer_list.dart';

class AccountView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      endDrawer: DrawerList(),
      appBar: AppBarWidget(),
      body: Center(
        child: Text("account"),
      ),
    );
  }
}
