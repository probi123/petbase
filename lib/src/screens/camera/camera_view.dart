import 'dart:async';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
// import 'package:path_provider/path_provider.dart';

List<CameraDescription> cameras;

class CameraView extends StatefulWidget {
  @override
  _CameraViewState createState() => _CameraViewState();
}

class _CameraViewState extends State<CameraView> {
  CameraController controller;
  File image;

  Future<void> picker() async {
    File pickedImage = await ImagePicker.pickImage(source: ImageSource.gallery);

    if (pickedImage != null) {
      image = pickedImage;
      setState(() {
        Navigator.pop(context, pickedImage);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    // if (!controller.value.isInitialized) {
    //   return Container();
    // }
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Image Picker'),
        ),
        body: Container(
          child: Center(
            child:
                image == null ? Text('No Image to Show ') : Image.file(image),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: picker,
          child: Icon(Icons.camera_alt),
        ),
      ),
    );
  }
}
