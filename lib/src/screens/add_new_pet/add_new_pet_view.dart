import 'package:flutter/material.dart';
import 'package:pawfinder/src/util/lang/pawfinder_localizations.dart';
import 'package:pawfinder/src/widgets/form/app_drop_down_button.dart';
import 'package:pawfinder/src/widgets/form/app_raised_button.dart';
import 'package:pawfinder/src/widgets/form/text_form_input_icon_outilne.dart';
import 'package:pawfinder/src/widgets/form/text_form_input_outline.dart';
import 'package:pawfinder/src/widgets/form/text_form_input_prefix_icon_outilne.dart';

enum InputDecorationType { NORMAL, ICON, PREFIXICON }

class AddNewPetView extends StatelessWidget {
  // Members:
  static const double _imageWidth = 300;
  static const double _imageHeight = 200;
  final photoUrl =
      'https://www.1stinflowers.com/pics/articles/poisonous/cat-sleeping-in-plant.jpg';

  // Build method:
  @override
  Widget build(BuildContext context) {
    return /* Scaffold(
        appBar: AppBarWidget(),
        bottomNavigationBar: BottomBar(),
        body: */
        Padding(
            padding: EdgeInsets.only(top: 20, left: 30, right: 30),
            child: Form(
              child: ListView(
                children: <Widget>[
                  Center(
                      child: Container(
                          margin: EdgeInsets.only(bottom: 20),
                          child: Text(
                            'DODAJ OGŁOSZENIE',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 30,
                                color: Theme.of(context).accentColor),
                          ))),
                  Center(
                      child: Image.network(
                    photoUrl,
                    height: _imageHeight,
                    width: _imageWidth,
                  )),
                  Container(
                      padding: EdgeInsets.only(top: 20),
                      child: TextFormInputOutline(label: 'Title')),
                  Container(
                    padding: EdgeInsets.only(top: 20),
                    child: Row(
                      children: <Widget>[
                        Flexible(
                            child: TextFormInputPrefixIconOutline(
                          label: "Location",
                          icon: Icons.pin_drop,
                        )),
                        Flexible(
                            child: TextFormInputPrefixIconOutline(
                          label: "Date",
                          icon: Icons.pin_drop,
                        )),
                      ],
                    ),
                  ),
                  Container(
                      padding: EdgeInsets.only(top: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Expanded(
                              child: AppDropDownButton(
                            optionsArray: ['Category'],
                            onChangedFuntion: (dynamic val) {},
                          )),
                          Expanded(
                              child: AppDropDownButton(
                            optionsArray: ['State'],
                            onChangedFuntion: (dynamic val) {},
                          )),
                        ],
                      )),
                  Container(
                      padding: EdgeInsets.only(top: 20),
                      child: TextFormInputIconOutline(
                          label: 'Description', icon: Icons.info, lines: 4)),
                  Container(
                    padding: EdgeInsets.only(top: 20),
                    child: Column(
                      children: <Widget>[
                        SwitchListTile(
                          value: true,
                          title: Text(
                            PawfinderLocalizations.of(context).getTranslation(
                                "I want my phone number to be visible"),
                            style: TextStyle(fontSize: 15),
                          ),
                          onChanged: (bool value) {},
                        ),
                        SwitchListTile(
                          value: true,
                          title: Text(
                            PawfinderLocalizations.of(context).getTranslation(
                                "I want my email to be visible"),
                            style: TextStyle(fontSize: 15),
                          ),
                          onChanged: (bool value) {},
                        )
                      ],
                    ),
                  ),
                  Container(
                      padding: EdgeInsets.only(top: 20),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            AppRaisedButton(
                              label: 'Cancel',
                              onPressed: () {},
                            ),
                            AppRaisedButton(
                              label: 'Accept',
                              onPressed: () {},
                            ),
                          ])),
                ],
              ),
              // )
            ));
  }
}
