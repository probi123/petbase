import 'package:flutter/material.dart';
import 'package:pawfinder/src/screens/add_new_pet/add_new_pet_view.dart';
import 'package:pawfinder/src/screens/all/not_filtered_view.dart';
import 'package:pawfinder/src/screens/discover/discover_view.dart';
import 'package:pawfinder/src/screens/filtered/filtered_view.dart';
import 'package:pawfinder/src/widgets/app_bar_widget.dart';
import 'package:pawfinder/src/widgets/bottom_navigation_bar.dart';

class MainView extends StatefulWidget {
  @override
  _MainViewState createState() => _MainViewState();
}

class _MainViewState extends State<MainView> {
  int _currentIndex = 0;
  final _views = [
    ["Home", FilteredView(), Icon(Icons.pets)],
    ["All", NotFiltered(), Icon(Icons.all_inclusive)],
    ["New", AddNewPetView(), Icon(Icons.add_circle_outline)],
    ["Discover", DiscoverView(), Icon(Icons.navigation)],
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBarWidget(),
        bottomNavigationBar: BottomBar(
          currentIndex: _currentIndex,
          onTap: onTapHandler,
          views: _views,
        ),
        body: _views[_currentIndex][1]);
  }

  void onTapHandler(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
}
