import 'package:flutter/material.dart';
import 'package:pawfinder/src/model/announcement/pet_announcement.dart';
import 'package:pawfinder/src/widgets/app_bar_widget.dart';
import 'package:pawfinder/src/widgets/drawer_list.dart';

class PetAnnouncementView extends StatelessWidget {
  final PetAnnouncement announcement;
  final double _objectsPadding = 10.0;
  final double _textOffset = 0.5;

  PetAnnouncementView({this.announcement});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      endDrawer: DrawerList(),
      appBar: AppBarWidget(),
      body: Center(
        child: Padding(
            padding: EdgeInsets.all(_objectsPadding),
            child: ListView(
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.only(bottom: _objectsPadding),
                    child: ClipRRect(
                        borderRadius: BorderRadius.circular(8.0),
                        child: Image.network(
                          announcement.photoUrl,
                        ))),
                Padding(
                  padding: EdgeInsets.only(
                      top: _objectsPadding, left: _objectsPadding),
                  child: Text(
                    announcement.title.toUpperCase(),
                    style: TextStyle(
                        shadows: [
                          Shadow(
                              // bottomLeft
                              offset: Offset(-_textOffset, -_textOffset),
                              color: Colors.black),
                          Shadow(
                              // bottomRight
                              offset: Offset(_textOffset, -_textOffset),
                              color: Colors.black),
                          Shadow(
                              // topRight
                              offset: Offset(_textOffset, _textOffset),
                              color: Colors.black),
                          Shadow(
                              // topLeft
                              offset: Offset(-_textOffset, _textOffset),
                              color: Colors.black),
                        ],
                        fontWeight: FontWeight.bold,
                        fontSize: 30,
                        // fontFamily: 'Lobster',
                        color: const Color(0xFF89CDC4)),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      bottom: _objectsPadding, left: _objectsPadding),
                  child: Row(children: <Widget>[
                    Icon(Icons.schedule),
                    Text(
                      announcement.timeLabel,
                      textScaleFactor: 0.9,
                    )
                  ]),
                ),
                Card(
                    child: Padding(
                        padding: EdgeInsets.all(_objectsPadding),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              // User button:
                              FlatButton(
                                  onPressed: () => Navigator.pushNamed(
                                      context, '/user/${announcement.id}'),
                                  child: Column(children: <Widget>[
                                    Icon(Icons.account_circle, size: 40),
                                    Text(announcement.userLabel),
                                  ])),
                              // Location button:
                              FlatButton(
                                  onPressed: () => Navigator.pushNamed(context,
                                      '/location/${announcement.location.long}/${announcement.location.lat}'),
                                  child: Column(children: <Widget>[
                                    Icon(
                                      Icons.location_on,
                                      size: 40,
                                    ),
                                    Text(announcement.locationLabel),
                                  ])),
                            ]))),
                Card(
                    child: Padding(
                        padding: EdgeInsets.all(_objectsPadding),
                        child: Text(announcement.description))),
                Container(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(announcement.comments[0].description)
                        // TODO: need to display comments (make commentViewWidget)
                      ]),
                )
              ],
            )),
      ),
    );
  }
}
