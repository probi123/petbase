import 'package:flutter/material.dart';
import 'package:pawfinder/src/util/mixins/validation_mixin.dart';
import 'package:pawfinder/src/util/services/auth_service.dart';
import 'package:pawfinder/src/widgets/form/app_raised_button.dart';
import 'package:pawfinder/src/widgets/form/text_form_input_outline.dart';

class RegisterView extends StatefulWidget {
  createState() => RegisterViewState();
}

class RegisterViewState extends State<RegisterView> with ValidationMixin {
  final _formKey = GlobalKey<FormState>();
  AuthService _authService;
  String _login = '';
  String _password = '';
  String _email = '';

  RegisterViewState() {
    _authService = new AuthService();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Container(
        margin: EdgeInsets.all(20.0),
        child: Form(
            key: _formKey,
            child: ListView(children: <Widget>[
              Column(
                children: <Widget>[
                  Text(
                    "PawFinder",
                    style: TextStyle(
                        fontFamily: 'Lobster',
                        color: Theme.of(context).accentColor),
                    textScaleFactor: 4.0,
                  ),
                  SizedBox(height: 20),
                  emailField(),
                  SizedBox(height: 20),
                  loginField(),
                  SizedBox(height: 20),
                  passwordField(),
                  SizedBox(height: 20),
                  submitButton(),
                  SizedBox(height: 20),
                ],
              ),
            ])),
      ),
    ));
  }

  Widget emailField() {
    return TextFormInputOutline(
      keyboardType: TextInputType.emailAddress,
      label: 'email_label',
      hintText: "you@pawfinder.com",
      validator: emailValidator,
      onSaved: (String value) {
        _email = value;
      },
    );
  }

  Widget loginField() {
    return TextFormInputOutline(
      keyboardType: TextInputType.emailAddress,
      label: 'login_label',
      hintText: "BestPawFinderEver",
      validator: loginValidator,
      onSaved: (String value) {
        _login = value;
      },
    );
  }

  Widget passwordField() {
    return TextFormInputOutline(
      obscureText: true,
      label: 'password_label',
      validator: passwordValidator,
      onSaved: (String value) {
        _password = value;
      },
    );
  }

  Widget submitButton() {
    return AppRaisedButton(
        onPressed: () {
          if (_formKey.currentState.validate()) {
            _formKey.currentState.save();
            if (_authService.register(_login, _email, _password)) {
              Navigator.pushReplacementNamed(context, '/home');
            }
          }
        },
        label: 'register_button');
  }
}
