import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'dart:async';
import 'package:pawfinder/pawfinder_exceptions.dart';
import 'package:pawfinder/src/util/lang/pawfinder_localizations.dart';
import 'package:pawfinder/src/util/mixins/validation_mixin.dart';
import 'package:pawfinder/src/util/services/auth_service.dart';
import 'package:pawfinder/src/widgets/form/app_raised_button.dart';
import 'package:pawfinder/src/widgets/form/text_form_input_outline.dart';
import 'package:pawfinder/src/scoped_models/main_model.dart';
import 'package:pawfinder/src/scoped_models/main_model.dart';

class LoginView extends StatefulWidget {
  createState() => LoginViewState();
}

class LoginViewState extends State<LoginView> with ValidationMixin {
  final _formKey = GlobalKey<FormState>();
  AuthService _authService;
  String _login = '';
  String _password = '';
  bool isLoading = false;

  LoginViewState() {
    _authService = new AuthService();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: isLoading
          ? CircularProgressIndicator()
          : Container(
              margin: EdgeInsets.all(20.0),
              child: Form(
                  key: _formKey,
                  child: ListView(children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "PawFinder",
                          style: TextStyle(
                              fontFamily: 'Lobster',
                              color: Theme.of(context).accentColor),
                          textScaleFactor: 4.0,
                        ),
                        SizedBox(height: 20),
                        loginField(),
                        SizedBox(height: 20),
                        passwordField(),
                        SizedBox(height: 20),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            submitButton(),
                            SizedBox(width: 10),
                            registerButton(),
                          ],
                        )
                      ],
                    ),
                  ])),
            ),
    ));
  }

  Widget loginField() {
    return TextFormInputOutline(
      keyboardType: TextInputType.emailAddress,
      label: 'login_label',
      hintText: "you@pawfinder.com",
      validator: loginValidator,
      onSaved: (String value) {
        _login = value;
      },
    );
  }

  Widget passwordField() {
    return TextFormInputOutline(
      obscureText: true,
      label:
          PawfinderLocalizations.of(context).getTranslation('password_label'),
      validator: passwordValidator,
      onSaved: (String value) {
        _password = value;
      },
    );
  }

  Widget submitButton() {
    MainModel mainModel = new MainModel();

    return ScopedModel(
      model: mainModel,
      child: AppRaisedButton(
          onPressed: () {
            if (_formKey.currentState.validate()) {
              _formKey.currentState.save();
              setState(() {
                this.isLoading = true;
              });

              mainModel.login(_login, _password).then((_) {
                Navigator.pushReplacementNamed(context, "/home");
                this.isLoading = false;
              }).catchError((e, stackTrace) {
                showDialog(
                  context: context,
                  builder: (BuildContext context) {
                  return alertDialog(context);
                  },
                );
                setState(() {
                  this.isLoading = false;
                });
              });
            }
          },
          label: 'login_button'),
    );
  }

  Widget alertDialog(BuildContext context) {
    return AlertDialog(
                      title: new Text("Login failed!"),
                      content: new Text("You inserted wrong username or password."),
                      actions: <Widget>[
                        // usually buttons at the bottom of the dialog
                        new FlatButton(
                          child: new Text("Close"),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ],
                    );
  }

  Widget registerButton() {
    return AppRaisedButton(
      onPressed: () {
        Navigator.pushNamed(context, '/register');
      },
      label: 'register_button',
    );
  }
}
