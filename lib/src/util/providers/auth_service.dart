import 'dart:convert';
import 'dart:async';
import 'package:http/http.dart';
import 'package:pawfinder/settings.dart';

final apiUrl = 'url/api';

class AuthService {
  final Client client = Client();

  authenticate(String login, String password) async {
    
    Map<String, String> header = {"Accept": "application/json" }; 

    dynamic response = await client.post(
      'https://find-pet-app.herokuapp.com/auth', 
      headers: {"Accept": "application/json"},
      body: jsonEncode({"login": login, "password": password})
    );

    print(response.body);

  }

  register(String login, String email, String password) {
    return true;
  }
}
