import 'dart:async';
 
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart' show SynchronousFuture;
import 'pawfinder_localizations.dart';
 
class PawfinderLocalizationsDelegate extends LocalizationsDelegate<PawfinderLocalizations> {
  const PawfinderLocalizationsDelegate();
 
  @override
  bool isSupported(Locale locale) => ['pl', 'en'].contains(locale.languageCode);
 
  @override
  Future<PawfinderLocalizations> load(Locale locale) => SynchronousFuture<PawfinderLocalizations>(PawfinderLocalizations(locale));
 
  @override
  bool shouldReload(LocalizationsDelegate<PawfinderLocalizations> old) => false;
}