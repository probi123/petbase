import 'package:flutter/material.dart';

class PawfinderLocalizations {
  final Locale locale;

  PawfinderLocalizations(this.locale);

  static PawfinderLocalizations of(BuildContext context) {
    return Localizations.of<PawfinderLocalizations>(
        context, PawfinderLocalizations);
  }

  String getTranslation(String key) {
    return _localizedValues[locale.languageCode].containsKey(key)
        ? _localizedValues[locale.languageCode][key]
        : key;
  }

  static Map<String, Map<String, String>> _localizedValues = {
    'pl': {
      'login_button': 'Zaloguj!',
      'password_label': 'Hasło',
      'email_label': 'Email',
      'login_label': 'Login',
      'register_button': 'Załóż konto',
      'my_account': 'Moje konto',
      'main_page': 'Strona Główna',
      'lost_pets': 'Zaginione',
      'found_pets': 'Znalezione',
      'seen_pets': 'Widziane',
      'logout': 'Wyloguj się',
      'help': 'Pomoc',
      'Title': 'Tytuł',
      'Description': 'Opis',
      'Location': 'Lokalizacja',
      'Date': 'Data',
      'Accept': 'Zatwierdź',
      'Cancel': 'Anuluj',
      'Category': "Kategoria",
      'State': "Stan",
      'I want my phone number to be visible':
          'Chcę, żeby mój numer telefonu był widoczny',
      'I want my email to be visible': 'Chcę, żeby mój email był widoczny',
    },
    'en': {'login_button': 'Login'}
  };
}
