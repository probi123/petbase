import 'dart:convert';
import 'dart:io';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:http/http.dart' as http;
import 'package:pawfinder/settings.dart';
import 'package:pawfinder/src/model/user/user.dart';

class UserService {

  Future<User> getById(int id) async {
    
    SharedPreferences pref = await SharedPreferences.getInstance();
    String token = pref.getString(Settings.TOKEN_KEY);
    
    http.Response response = await http
        .get('${Settings.USER_REST_URL}$id', headers: {
      "Accept": "application/json",
      HttpHeaders.authorizationHeader: token
    });
    
    Map<String, dynamic> responseBody = json.decode(response.body);
    print(responseBody);
    
    User user = new User(
        id: responseBody["id"],
        userName: responseBody["userName"],
        email: responseBody['email'],
        password: responseBody['password']);
    
    return user;
  }

  saveUser(User user) async {
    var json = user.toJson();
    final response = await http
        .post('${Settings.ANNOUNCMENT_REST_URL}/users/new', body: json);
  }
}
