import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:pawfinder/settings.dart';
import 'package:pawfinder/src/model/announcement/comment.dart';

class CommentService {
  Future<Comment> findCommentById(int commentId) async {
    final response = await http.get(
      Settings.COMMENT_REST_URL + '$commentId',
      headers: {HttpHeaders.authorizationHeader: Settings.JWT_TOKEN},
    );

    final responseBody = json.decode(response.body);
    print(responseBody);

    Comment comment = Comment.fromJsonWithDefaultDate(responseBody);
    print(comment);

    return comment;
  }

  Future<List<Comment>> findAllCommentsForAnnouncementId(
      int announcementId) async {
    final response = await http.get(
      Settings.COMMENT_REST_URL + 'announcementId/$announcementId',
      headers: {HttpHeaders.authorizationHeader: Settings.JWT_TOKEN},
    );

    final responseBody = json.decode(response.body);
    print(responseBody);

    List<Comment> comments = _getListOfComments(responseBody);
    print(comments);

    return comments;
  }

  List<Comment> _getListOfComments(responseBody) {
    List<Comment> list = List<Comment>();
    for (int i = 0; i < responseBody.length; i++) {
      list.add(Comment.fromJsonWithDefaultDate(responseBody[i]));
    }
    return list;
  }

  Future<int> countAmountOfCommentsForSpecificAnnouncementId(
      int announcementId) async {
    final response = await http.get(
      Settings.COMMENT_REST_URL + '$announcementId/amount',
      headers: {HttpHeaders.authorizationHeader: Settings.JWT_TOKEN},
    );

    final responseBody = json.decode(response.body);
    print(responseBody);

    return responseBody as int;
  }
}
