import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:pawfinder/settings.dart';
import 'package:pawfinder/src/model/announcement/pet_announcement.dart';
import 'package:pawfinder/src/scoped_models/main_model.dart';

class AnnouncementService {
  Future<PetAnnouncement> findById(int id) async {
    final response = await http.get(
      Settings.ANNOUNCMENT_REST_URL + '$id',
      headers: {HttpHeaders.authorizationHeader: Settings.JWT_TOKEN},
    );

    final responseJson = json.decode(response.body);
    print(responseJson);

    PetAnnouncement announcement = PetAnnouncement.fromJson(responseJson);
    print(announcement);

    return announcement;
  }

  Future<List<PetAnnouncement>> findAll() async {
    final response = await http.get(
      Settings.ANNOUNCMENT_REST_URL + 'all',
      headers: {HttpHeaders.authorizationHeader: MainModel().token},
    );

    final responseBody = json.decode(response.body);
    print(responseBody);

    List<PetAnnouncement> announcements = [];

    for (var announcementBody in responseBody) {
      announcements.add(new PetAnnouncement.fromJson(announcementBody));
    }

    print(announcements.length);

    return announcements;
  }

  findAllStatuses() {
    return _findAllUtils('status');
  }

  findAllTypes() {
    return _findAllUtils('types');
  }

  findAllColors() {
    return _findAllUtils('color');
  }

  Future<List<String>> _findAllUtils(String util) async {
    final response = await http.get(
      Settings.ANNOUNCMENT_REST_URL + util,
      headers: {HttpHeaders.authorizationHeader: Settings.JWT_TOKEN},
    );

    final responseBody = json.decode(response.body);
    print(responseBody);

    List<String> utils = List<String>();
    for (var util in responseBody[util]) {
      utils.add(util);
    }

    return utils;
  }

  Future<List> findLost() async {
    List<PetAnnouncement> announcementsList = await findAll();
    List<PetAnnouncement> lostList = announcementsList
        .where((element) => element.status == "zaginął" ? true : false).toList();
    return lostList;
  }

    Future<List> findFound() async {
    List<PetAnnouncement> announcementsList = await findAll();
    List<PetAnnouncement> lostList = announcementsList
        .where((element) => element.status == "znalazłem" ? true : false).toList();
    return lostList;
  }
}
