import 'dart:convert';
import 'package:pawfinder/pawfinder_exceptions.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import 'package:pawfinder/settings.dart';
import 'package:pawfinder/src/util/services/user_service.dart';
import 'package:pawfinder/src/model/user/user.dart';
import 'package:test/test.dart';

class AuthService {
  Future<Map<String, dynamic>> authenticate(
      String login, String password)  async { 
    /*  Function returns Map<String, dynamic> in Future wraper.
        Map structure is like:
        {
        "token" : "345nb3jh4j4b4jh2b3hb32...",
        "user" : User()
        }
    */
    Map<String, String> header = {"Accept": "application/json"};
    http.Response response = await http.post(Settings.AUTH_URL,
        headers: header,
        body: jsonEncode({"login": login, "password": password}));
    
    if(response.statusCode == 403) {
      throw("Forbiden");
    }

    Map<String, dynamic> responseBody = json.decode(response.body);

    String token = responseBody["Authorization"].toString().split(' ')[1];
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setString(Settings.TOKEN_KEY, token);

    int id = responseBody["id"];
    User user = await UserService().getById(id);

    return {"token": token, "user": user};
  }

  register(String login, String email, String password) {
    return true;
  }
}
