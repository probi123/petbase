class Settings {
  static String SERVER_URL = "https://find-pet-app.herokuapp.com/";
  static String USER_REST_URL = "https://find-pet-app.herokuapp.com/rest/user/";
  static String AUTH_URL = "https://find-pet-app.herokuapp.com/auth";
  static String ANNOUNCMENT_REST_URL =
      "https://find-pet-app.herokuapp.com/rest/announcement/";
  static String COMMENT_REST_URL =
      "https://find-pet-app.herokuapp.com/rest/comment/";
  static String JWT_TOKEN =
      "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJrcnp5c3p0b2YyIiwic2NvcGVzIjpbIkFETUlOIiwiVVNFUiJdLCJleHAiOjE1NjAyNTE3NDR9.Sb4x2UgOW-uvl0vV-QV3PUmsfOaxhAr4eS_9Vhi6LpNlUYXg4u1YWU1FtTFdKCWGos3t48EVpq7BCjCVTPtg_Q";

  static String TOKEN_KEY = "pawfinder_auth_user_token";
}
