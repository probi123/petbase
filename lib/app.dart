import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:pawfinder/src/screens/account/account_view.dart';
import 'package:pawfinder/src/screens/add_new_pet/add_new_pet_view.dart';
import 'package:pawfinder/src/screens/all/not_filtered_view.dart';
import 'package:pawfinder/src/screens/auth/login_view.dart';
import 'package:pawfinder/src/screens/auth/register_view.dart';
import 'package:pawfinder/src/screens/discover/discover_view.dart';
import 'package:pawfinder/src/screens/filtered/filtered_view.dart';
import 'package:pawfinder/src/screens/main_view.dart';
import 'package:pawfinder/src/screens/pet_announcement/pet_announcement_view.dart';
import 'package:pawfinder/src/util/lang/pawfinder_localizations_delegate.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowMaterialGrid: false,
      theme: ThemeData(
          primaryColor: Colors.white,
          accentColor: const Color(0xFF89CDC4),
          backgroundColor: const Color(0xFF89CDC4),
          accentIconTheme: IconThemeData(color: Colors.white)),
          home: LoginView(),
      // home: MainView(),
      localizationsDelegates: [
        const PawfinderLocalizationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate
      ],
      supportedLocales: [
        const Locale('en', 'US'),
        const Locale('pl', 'PL'),
      ],
      routes: {
        '/all': (BuildContext context) => NotFiltered(),
        // '/home': (BuildContext context) => FilteredView(
        //       initialTabIndex: 0,
        //     ),
        '/home': (BuildContext context) => MainView(),
        '/register': (BuildContext context) => RegisterView(),
        '/filtered/lost': (BuildContext context) => FilteredView(
            // initialTabIndex: 0,
            ),
        '/filtered/found': (BuildContext context) => FilteredView(
            // initialTabIndex: 1,
            ),
        '/filtered/seen': (BuildContext context) => FilteredView(
            // initialTabIndex: 2,
            ),
        '/account': (BuildContext context) => AccountView(),
        '/discover': (BuildContext context) => DiscoverView(),
        '/notification': (BuildContext context) => AddNewPetView(),
      },
      onUnknownRoute: (RouteSettings settings) => MaterialPageRoute(
          builder: (BuildContext context) => FilteredView(
              // initialTabIndex: 0,
              )),
      onGenerateRoute: (RouteSettings settings) {
        final List<String> pathElements = settings.name.split('/');
        if (pathElements[0] != '') {
          return null;
        }

        if (pathElements[1] == 'announcement') {
          return MaterialPageRoute(
              builder: (BuildContext context) => PetAnnouncementView(
                    announcement: settings.arguments,
                  ));
        }
      },
    );
  }
}
